use std::cell::RefCell;
use std::rc::Rc;

use yew::prelude::*;
use yewdux::prelude::*;
use yewtil::NeqAssign;
use yew::prelude::worker::HandlerId;
use yewdux::store::reducer::Reducer;


pub struct AppStore<T>
    where
        T: 'static + Reducer + Clone + Default,
{
    state: Rc<Rc<RefCell<T>>>,
}

impl<T: 'static + Reducer + Clone + Default> Store for AppStore<T> {
    type Model = Rc<RefCell<T>>;
    type Message = ();
    type Input = T::Action;
    type Output = ();

    fn new(_link: StoreLink<Self>) -> Self {
        Self {
            state: Default::default(),
        }
    }

    fn state(&mut self) -> &mut Rc<Self::Model> {
        &mut self.state
    }

    fn handle_input(&mut self, msg: Self::Input, _who: HandlerId) -> Changed {
        // IMPORTANT: This changes the outer Rc pointer, so subscribers can determine if state was
        // modified. Otherwise DispatchProps won't work.
        let state = Rc::make_mut(&mut self.state);
        state.borrow_mut().reduce(msg)
    }
}

