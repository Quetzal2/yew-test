use std::cell::RefCell;
use std::rc::Rc;

use yew::prelude::*;
use yewdux::prelude::*;
use yewtil::NeqAssign;


use yewdux::store::reducer::Reducer;

use crate::types::colour::Colour;
use pencil::Pencil;
use pencil::PencilAction;
use pencil::PencilType;
use grid::Grid;
use grid::GridAction;
use cell::Cell;
use toolbox::ToolBox;
use toolbox::ToolBoxAction;
use active_colour::ActiveColour;
use ui::UI;
use ui::UIAction;

pub mod active_colour;
pub mod pencil;
pub mod grid;
pub mod cell;
pub mod ui;
pub mod toolbox;

use log::debug;

#[derive(Clone)]
pub struct State {
    pub pencil: Pencil,
    pub toolbox: ToolBox,
    pub grid: Grid,
    pub ui: UI,
}

impl Default for State {
    fn default() -> Self {
        State {
            pencil: Pencil {
                active_pencil: PencilType::Colour,
            },
            toolbox: ToolBox {
                active_colour: ActiveColour {
                    colour: Colour::Green,
                }
            },
            grid: Grid {
                height: 5,
                width: 7,
                cell_size: 18,
                cells: Vec::from([Cell { colour: Colour::Indigo, pinned: false }; 35]),
            },
            ui: UI {
                pos: (70, 12),
                shown: true,
                dark: true,
            },
        }
    }
}

pub enum AppAction {
    UI(UIAction),
    ToolBox(ToolBoxAction),
    Grid(GridAction),
}

impl Reducer for State {
    type Action = AppAction;

    fn reduce(&mut self, action: Self::Action) -> bool {
        match action {
            AppAction::UI(action) => self.ui.reduce(action),
            AppAction::Grid(action) =>
                match action {
                    GridAction::Change(idx) => match self.pencil.active_pencil {
                        PencilType::Colour => {self.grid.cells[idx].colour = self.toolbox.active_colour.colour; true},
                        PencilType::Pin => {self.grid.cells[idx].pinned = !self.grid.cells[idx].pinned; true},
                    },
                    _ => self.grid.reduce(action),
                }
            _ => unimplemented!()
        }
    }

    fn new() -> Self {
        todo!()
    }
}


/*
impl rtr::Component for State {

}*/
