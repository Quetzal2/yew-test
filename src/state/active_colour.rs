use crate::types::colour::Colour;

#[derive(Default, Clone)]
pub struct ActiveColour {
    pub colour: Colour,
}