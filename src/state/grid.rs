use super::cell::Cell;
use yewdux::store::reducer::Reducer;

#[derive(Default, Clone)]
pub struct Grid {
    pub cells: Vec<Cell>,
    pub cell_size: usize,
    pub width: usize,
    pub height: usize,
}

pub enum GridAction {
    Change(usize)
}

impl Reducer for Grid {
    type Action = GridAction;

    fn reduce(&mut self, action: Self::Action) -> bool {
        match action {
            _=> unimplemented!()
        }
    }

    fn new() -> Self {
        todo!()
    }
}
