use yewdux::store::reducer::Reducer;
use debug_macro::debug as dbg;


#[derive(Default, Clone)]
pub struct UI {
    pub pos: (i32, i32),
    pub shown: bool,
    pub dark: bool,
}

pub enum UIAction {
    ToolBoxMoved(i32, i32)
}


impl Reducer for UI {
    type Action = UIAction;

    fn reduce(&mut self, action: Self::Action) -> bool {
        match action {
            UIAction::ToolBoxMoved(x, y) => {self.pos = (x, y) ; true},
            _=> unimplemented!()
        }
    }

    fn new() -> Self {
        todo!()
    }
}

