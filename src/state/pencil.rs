use std::fmt::{Display, Formatter, Result};

#[derive(Default, Clone)]
pub struct Pencil {
    pub active_pencil: PencilType,
}

#[derive(Clone)]
pub enum PencilType {
    Colour,
    Pin,
}

impl Default for PencilType {
    fn default() -> Self {
        PencilType::Colour
    }
}

impl Display for PencilType {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        let pencil_type = match self {
            PencilType::Pin => "Pin",
            PencilType::Colour => "Colour",
        };
        write!(f, "{}", pencil_type)
    }
}


impl PartialEq for PencilType {
    fn eq(&self, other: &Self) -> bool {
        self.to_string() == other.to_string()
    }
}

impl PencilType {
    pub fn enumerate() -> [PencilType; 2] {
        [Self::Colour, Self::Pin]
    }
}


pub enum PencilAction {
    Change(PencilType)
}