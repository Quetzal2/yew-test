use super::active_colour::ActiveColour;

#[derive(Default, Clone)]
pub struct ToolBox {
    pub active_colour: ActiveColour,
}

pub enum ToolBoxAction {
}