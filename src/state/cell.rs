use crate::types::colour::Colour;

#[derive(Default, Clone, Copy)]
pub struct Cell {
    pub colour: Colour,
    pub pinned: bool,
}