use super::active_colour::ActiveColour;
use yew::{ComponentLink, Html, prelude::html, Properties, Component, Children, PointerEvent, NodeRef, Callback};
use web_sys::HtmlDivElement;

pub struct ToolBox {
    drag: Option<DragState>,
    tool_ref: NodeRef,
    props: Props,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub onmoved: Callback<(i32, i32)>,
    #[prop_or_default]
    pub children: Children,
}

#[derive(Default)]
struct DragState {
    offset_x: i32,
    offset_y: i32,
}

pub enum Msg {
    MoveTake(PointerEvent),
    Move(PointerEvent),
    MoveDrop(PointerEvent),
}

impl yew::Component for ToolBox {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {props, link, drag: None, tool_ref: NodeRef::default()}
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Self::Message::MoveTake(event) => {
            self.tool_ref.cast::<HtmlDivElement>().unwrap().class_list().add_1("ev--move").unwrap();
            self.drag = Some(DragState {offset_x: event.client_x(), offset_y: event.client_y()});
            },
            Self::Message::Move(event) if self.drag.is_some() => {
                let ox = self.tool_ref.cast::<HtmlDivElement>().unwrap().offset_left();
                let oy = self.tool_ref.cast::<HtmlDivElement>().unwrap().offset_top();
                let dx = self.drag.as_mut().unwrap().offset_x - event.client_x();
                let dy = self.drag.as_mut().unwrap().offset_y - event.client_y();
                self.tool_ref.cast::<HtmlDivElement>().unwrap().style().set_property("left", ((ox-dx).to_string() + "px").as_str()).unwrap();
                self.tool_ref.cast::<HtmlDivElement>().unwrap().style().set_property("top", ((oy-dy).to_string() + "px").as_str()).unwrap();
                self.drag.as_mut().unwrap().offset_x = event.client_x();
                self.drag.as_mut().unwrap().offset_y = event.client_y();

            },
            Self::Message::MoveDrop(event) if self.drag.is_some() => {
                self.tool_ref.cast::<HtmlDivElement>().unwrap().class_list().remove_1("ev--move").unwrap();
                self.drag = None;
                self.props.onmoved.emit(
                    (self.tool_ref.cast::<HtmlDivElement>().unwrap().offset_left(),
                    self.tool_ref.cast::<HtmlDivElement>().unwrap().offset_top())
                );
            },
            _ => {}
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        true
    }

    fn view(&self) -> Html {
        let drag_start = self.link.callback(Msg::MoveTake);
        let drag_move = self.link.callback(Msg::Move);
        let drag_stop = self.link.callback(Msg::MoveDrop);
        html! {
            <div ref=self.tool_ref.clone()
              class="toolbox_cmp border border-gray-700 dark:border-gray-700 bg-warmGray-800 dark:bg-warmGray-800" style="display: block; position: absolute; padding: 20px;"
              onpointerdown=drag_start onpointerup=drag_stop onpointermove=drag_move>
                <h2 style="margin-top: -10px;">{"Toolbox"}</h2>
                { for self.props.children.iter() }
            </div>
        }
    }
}