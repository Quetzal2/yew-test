use super::cell::Cell;
use yew::{ComponentLink, prelude::html, Html, Component, Properties, Children, Callback, ChildrenWithProps};
use yew::virtual_dom::VChild;
use log::debug;
use debug_macro::debug as dbg;

pub struct Grid {
    pressed: bool,
    props: Props,
    link: ComponentLink<Self>,
}

#[derive(Properties, Clone)]
pub struct Props {
    pub oncolourchange: Callback<usize>,
    pub width: usize,
    pub height: usize,
    #[prop_or_default]
    pub children: ChildrenWithProps<Cell>,
}

pub enum Msg {
    Press(bool),
    Change(usize),
}

impl yew::Component for Grid {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {props, link, pressed: false}
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Press(pressed) => {self.pressed = pressed; false},
            Msg::Change(idx) => if self.pressed {self.props.oncolourchange.emit(idx); true} else {false},
        }
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.props = props;
        true
    }

    fn view(&self) -> Html {
        let down = self.link.callback(|_| Msg::Press(true));
        let up = self.link.callback(|_| Msg::Press(false));
        html! {
            <div style="position: absolute;\
  top: 50%;\
  left: 50%;\
  transform: translate(-50%, -50%);"
            onpointerdown=down onpointerup=up>
                <div
                  class="grid_cmp border border-gray-900 bg-warmGray-800 dark:border-gray-900 dark:bg-warmGray-800" style="display: grid;\
                    position: absolute;\
                    grid-template-columns: repeat(".to_string()+self.props.width.to_string().as_str()+", 1fr);\
                    /*gap: 10px;*/\
                    grid-template-rows: repeat("+self.props.height.to_string().as_str()+", 1fr);\
                    width: auto; height: auto;\
                    padding: 10px;">
                    { for self.props.children.iter().map(|mut cell| {cell.props.grid_link = Some(self.link.clone()); cell}) }
                </div>
            </div>
        }
    }
}