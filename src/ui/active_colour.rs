use crate::types::colour::Colour;
use yew::{ComponentLink, prelude::html, Html, Properties, Component};

#[derive(Default)]
pub struct ActiveColour {
    colour: Colour,
}

impl yew::Component for ActiveColour {
    type Message = ();
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Default::default()
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        todo!()
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        true
    }

    fn view(&self) -> Html {
        html! {
            <select class="text-gray-600 active_colour_cmp">
                { Colour::enumerate().iter().map(|name| if *name == self.colour {html!{<option selected=true>{name.to_string()}</option>}} else {html!{<option>{name.to_string()}</option>}}).collect::<Html>() }
            </select>
        }
    }
}