use crate::state::pencil::PencilType;
use yew::{ComponentLink, Html, Component, html};
use yew::html::Properties;

#[derive(Default)]
pub struct ActivePencil {
    pencil: PencilType,
}


impl yew::Component for ActivePencil {
    type Message = ();
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Default::default()
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        todo!()
    }

    fn change(&mut self, _props: Self::Properties) -> bool {
        true
    }

    fn view(&self) -> Html {
        html! {
            <select class="text-gray-600 active_colour_cmp">
                { PencilType::enumerate().iter().map(|name| if *name == self.pencil {html!{<option selected=true>{name.to_string()}</option>}} else {html!{<option>{name.to_string()}</option>}}).collect::<Html>() }
            </select>
        }
    }
}