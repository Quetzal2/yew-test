pub mod cell;
pub mod active_colour;
pub mod active_pencil;
pub mod cursor;
pub mod grid;
pub mod toolbox;

use cell::Cell;
use grid::Grid;
use toolbox::ToolBox;
use active_colour::ActiveColour;
use active_pencil::ActivePencil;
use cursor::Cursor;
use yew::{ComponentLink, prelude::html, Html, Properties, Component, ShouldRender, NodeRef};
use yewdux::prelude::*;
use crate::store::AppStore;
use crate::state::{State, AppAction};
use yewtil::NeqAssign;
use crate::state::ui::UIAction;
use crate::state::grid::GridAction;
use yew::html_nested;

use log::{trace, debug};
use debug_macro::debug as dbg;

pub struct UI {
    grid_ref: NodeRef,
    dispatch: DispatchProps<AppStore<State>>,
}


pub enum Msg {
    SwitchTheme,
    Move(usize, usize),

}

#[derive(Properties, Clone)]
pub struct Props {
    #[prop_or_default]
    pub dispatch: DispatchProps<AppStore<State>>,
}
impl DispatchPropsMut for Props {
    type Store = AppStore<State>;

    fn dispatch(&mut self) -> &mut DispatchProps<Self::Store> {
        &mut self.dispatch
    }
}


impl yew::Component for UI {
    type Message = Msg;
    type Properties = Props;

    fn create(dispatch: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { dispatch: dispatch.dispatch, grid_ref: NodeRef::default() }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        todo!()
    }

    fn change(&mut self, handle: Self::Properties) -> ShouldRender {
        true//self.dispatch.neq_assign(handle.dispatch)
    }

    fn view(&self) -> Html {
        use yew::virtual_dom::VChild;

        let size = self.dispatch.state().borrow().grid.cell_size.to_string();
        let moved = self.dispatch.callback(|(x, y)| AppAction::UI(UIAction::ToolBoxMoved(x, y)));
        let changecolour = self.dispatch.callback(|idx| AppAction::Grid(GridAction::Change(idx)));
        html! {
            <div
            class="ui_cmp text-warmGray-100 bg-warmGray-900 dark:text-warmGray-100 dark:bg-warmGray-900 wide" style="position: relative;">
                <Grid
            width={self.dispatch.state().borrow().grid.width} height={self.dispatch.state().borrow().grid.height}
                  oncolourchange=changecolour>
                    { for self.dispatch.state().borrow().grid.cells.iter().enumerate().map(|(idx, cell)| html_nested! {
                        <Cell
                          colour=cell.colour.clone()
                          pinned=cell.pinned
                          idx=idx
                          grid_link=None
                          css_style={"width: ".to_string()+size.as_str()+"px; height:"+size.as_str()+"px;"}/>
                    }) }
                </Grid>
                <ToolBox onmoved=moved>
                    <ActivePencil/>
                    <ActiveColour/>
                </ToolBox>
            </div>
        }
    }
}