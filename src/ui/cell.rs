use yew::{self, prelude::html, ComponentLink, Html, Properties, Callback, NodeRef};
use crate::types::colour::Colour;
use crate::ui::grid::Grid;
use crate::ui::grid;
use log::{debug, trace};

#[derive(Default)]
pub struct Cell {
    props: Props,
}

#[derive(Properties, Clone, Default)]
pub struct Props {
    pub grid_link: Option<ComponentLink<Grid>>,
    pub colour: Colour,
    pub pinned: bool,
    pub css_style: String,
    pub idx: usize,
}



impl yew::Component for Cell {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self {props}
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        true
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.props = props;
        true
    }

    fn view(&self) -> Html {
        let idx = self.props.idx;
        let idx_str = idx.to_string();
        let onhover = self.props.grid_link.as_ref().unwrap().callback(move |_| grid::Msg::Change(idx));
        html! {
            <div
            class="cell_cmp"
            style={"background-color: ".to_string() + self.props.colour.to_string().as_str() + ";
            " + self.props.css_style.as_str() + "border: solid black "+ if self.props.pinned {"2"} else {"0"} +"px;
            display: inline-grid;"}
            onpointerover=onhover >
            {if  self.props.pinned {"⬤"} else {/*idx_str.as_str()*/""}}
            </div>
        }
    }
}