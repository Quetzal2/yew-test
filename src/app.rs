use yew::prelude::*;
use yewdux::prelude::*;
use crate::state::State;
use crate::store::{AppStore};
use yewtil::NeqAssign;
use crate::ui::UI;
use yewdux::store::reducer::Reducer;

pub struct App {
    dispatch: DispatchProps<AppStore<State>>,
}

impl Component for App {
    type Message = ();
    type Properties = DispatchProps<AppStore<State>>;

    fn create(dispatch: Self::Properties, _link: ComponentLink<Self>) -> Self {
        Self { dispatch }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, handle: Self::Properties) -> ShouldRender {
        true//self.dispatch.neq_assign(handle)
    }

    fn view(&self) -> Html {
        //let state = self.dispatch.state().borrow();
        //let double = self.dispatch.reduce_callback(|s| s.borrow_mut().count *= 2);
        /*html! {
            <p class="bg-red-500">{ "Hello world!" }</p>
        }<>
            <h1>{ count }</h1>
            <button onclick=incr>{"+1"}</button>
            <button onclick=double>{"x2"}</button>
            </>
        }*/
        html! {
            <div id="dark_body" class="dark wide" >
                <div class="text-gray-100 bg-gray-900 dark: text-gray-100 dark:bg-gray-900 wide">
                    <WithDispatch<UI>/>
                </div>
            </div>
        }
    }
}


type Application = WithDispatch<App>;

fn main() {
    yew::start_app::<Application>();
}
