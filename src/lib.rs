#![recursion_limit="1024"]

use wasm_bindgen::prelude::*;

use console_log;
use fern;

use app::App;
use yewdux::prelude::WithDispatch;

mod app;

mod types;
mod state;
mod ui;
pub mod store;

#[wasm_bindgen]
pub fn run_app() -> Result<(), JsValue> {
    logging();

    yew::start_app::<WithDispatch<App>>();

    Ok(())
}


fn logging() {
    fern::Dispatch::new()
        .chain(fern::Output::call(console_log::log))
        .apply().unwrap();
}