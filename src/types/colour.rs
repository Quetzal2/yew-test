use std::fmt::{Display, Formatter, Result};

#[derive(Clone, Copy, Debug)]
pub enum Colour {
    Red,
    Orange,
    Yellow,
    Green,
    Blue,
    Indigo,
    Purple,
    White,
    Black,
    None,
}

impl Default for Colour {
    fn default() -> Self {
        Self::None
    }
}

impl Display for Colour {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        let colour = match self {
            Self::Red => "red",
            Self::Orange => "orange",
            Self::Yellow => "yellow",
            Self::Green => "green",
            Self::Blue => "blue",
            Self::Indigo => "indigo",
            Self::Purple => "purple",
            Self::White => "white",
            Self::Black => "black",
            Self::None => "inherit",
        };
        write!(f, "{}", colour)
    }
}

impl PartialEq for Colour {
    fn eq(&self, other: &Self) -> bool {
        self.to_string() == other.to_string()
    }
}

impl Colour {
    pub fn enumerate() -> [Colour;10] {
        [Self::Red, Self::Orange, Self::Yellow, Self::Green, Self::Blue, Self::Indigo, Self::Purple, Self::White, Self::Black, Self::None]
    }
}